use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::device::Device;
use vulkano::command_buffer::DynamicState;

use core::resources::bvh_builder::*;
use core::next_tree::content::ContentType;
use core::next_tree::{JakarNode, SaveUnwrap};
use render::uniform_manager::UniformManager;
use render::pipeline_manager::PipelineManager;
use render::shader::shader_inputs::ray_tracing;
use core::resources::mesh::Mesh;
use core::ReturnBoundInfo;

use tools::math::bound_tools::*;
use tools::math::time_tools::as_ms;
use tools::vec_tools::SubVec;

use collision::*;
use cgmath::*;
use std::sync::{Arc, RwLock};
use std::time::Instant;

impl BvhBuildable for Mesh{
    fn get_center<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Point3<f32>{
        self.get_bound().center()
    }
    fn get_aabb<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Aabb3<f32>{
        self.get_bound()
    }
    fn get_sah<T: BvhBuildable>(&self, buffer: &Vec<T>) -> f32{
        self.get_bound().surface_area() * self.get_triangle_count() as f32
    }

    fn get_triangle_count<T: BvhBuildable>(&self, buffer: &Vec<T>) -> u32{
        self.get_triangle_count() as u32
    }

    fn get_mesh_data<T: BvhBuildable>(&self, buffer: &Vec<T>) -> MeshDataSize{
        self.as_mesh_data()
    }
}

///Implementation for a mesh which is transformed by the matrix
impl BvhBuildable for (Matrix4<f32>, Arc<RwLock<Mesh>>){
    fn get_center<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Point3<f32>{
        self.0.transform_point(self.1.read().expect("failed to read mesh").get_center(buffer))
    }
    fn get_aabb<T: BvhBuildable>(&self, buffer: &Vec<T>) -> Aabb3<f32>{
        self.1.read().expect("failed to read mesh").get_bound().transform_bound(self.0)
    }
    fn get_sah<T: BvhBuildable>(&self, buffer: &Vec<T>) -> f32{
        let mesh_read = self.1.read().expect("failed to read mesh");
        mesh_read.get_aabb(buffer).surface_area() * mesh_read.get_triangle_count() as f32
    }

    fn get_triangle_count<T: BvhBuildable>(&self, buffer: &Vec<T>) -> u32{
        self.1.read().expect("failed to read mesh").get_triangle_count() as u32
    }

    fn get_mesh_data<T: BvhBuildable>(&self, buffer: &Vec<T>) -> MeshDataSize{
        self.1.read().expect("failed to read mesh").as_mesh_data()
    }

}


///Takes a list of meshes and generates an "optimal" bvh from them. (Generation are two steps. Ordering by morthon code,
/// then assambling based on the morthon code of the mesh AND a surface area heuristic of the subtrees).
#[derive(Clone)]
pub struct MeshBvh {
    root: Arc<BvhNode>,
    flat_bvh: Vec<ray_tracing::ty::BvhNode>,
    //Contains mesh specific data like the model matrix and material definition
    mesh_buffer: Vec<ray_tracing::ty::Mesh>,
    bottom_bvh_buffer: SubVec<ray_tracing::ty::BvhNode>,
    faces_buffer: SubVec<ray_tracing::ty::Face>,
    vertex_buffer: SubVec<ray_tracing::ty::Vertex>,
}


impl MeshBvh{
    /// Generates the bvh for this set of nodes which should all be meshes, otherwise the generation algorithm
    /// might produce a wrong bvh.
    pub fn new(meshes: Vec<JakarNode>) -> Self{

        //Prepare our info by creating a transform collection and a Arc<_> collection.
        let trans_per_node: Vec<(Matrix4<f32>, Arc<RwLock<Mesh>>)> = meshes.iter().filter_map(
            |node| {
                let transform = node.get_attrib().get_matrix();
                match node.get_value(){
                    ContentType::Mesh(ref mesh) => Some((transform, mesh.clone())),
                    _ => {
                        println!("WARNING, IS NO MESH", );
                        None
                    }
                }
            }
        ).collect();

        //Build a tree from the nodes we got
        let mut tree = build_tree(&trans_per_node);

        //Get us the size for the buffers
        let mut buffer_size = MeshDataSize{
            bottom_bvh_buffer_len: 0,
            mesh_buffer_len: 0,
            vertex_buffer: 0,
            faces_buffer: 0,
        };

        tree.to_mesh_data(&mut buffer_size);

        //now with the indexes in place we can transform this thingy into a glsl tree
        //with proper escape indexes in the array
        //We'll also need a vertex buffer, currently this is only a list of all triangle but later this
        //will be a more complex structure of verts and faces.
        let mut bottom_bvh_buffer: SubVec<ray_tracing::ty::BvhNode> = SubVec::new();
        let mut mesh_buffer: Vec<ray_tracing::ty::Mesh> = Vec::with_capacity(buffer_size.mesh_buffer_len);
        let mut faces_buffer: SubVec<ray_tracing::ty::Face> = SubVec::new();
        let mut vertex_buffer: SubVec<ray_tracing::ty::Vertex> = SubVec::new();


        let mut to_flat_debug = TimeDebug::new();

        let mut flat_tree = tree.to_flat_top(
            None,
            &mut mesh_buffer,
            &mut bottom_bvh_buffer,
            &mut faces_buffer,
            &mut vertex_buffer,
            &trans_per_node,
            //&mut to_flat_debug,
        );


        //to_flat_debug.print();

        //sort the flat tree
        flat_tree.sort_unstable_by(
            |node1, node2| node1.idx.cmp(&node2.idx)
        );

        //Store
        MeshBvh{
            root: tree,
            flat_bvh: flat_tree,
            mesh_buffer,
            bottom_bvh_buffer,
            faces_buffer,
            vertex_buffer,
        }
    }

    ///Helper function to make bound drawing easy
    pub fn draw_bounds(&self,
        command_buffer: AutoCommandBufferBuilder,
        pipeline_manager: Arc<RwLock<PipelineManager>>,
        device: Arc<Device>,
        uniform_manager: Arc<RwLock<UniformManager>>,
        dynamic_state: &DynamicState,
    ) -> AutoCommandBufferBuilder{
        self.root.draw_bound(
            1, //lvl 1 / for no DIV by 0
            command_buffer,
            pipeline_manager,
            device,
            uniform_manager,
            dynamic_state,
            None //in world space
        )
    }

    pub fn get_top_bvh(&self) -> Vec<ray_tracing::ty::BvhNode>{
        self.flat_bvh.clone()
    }

    pub fn get_bottom_bvh(&self) -> SubVec<ray_tracing::ty::BvhNode>{
        self.bottom_bvh_buffer.clone()
    }

    pub fn get_mesh_buffer(&self) -> Vec<ray_tracing::ty::Mesh>{
        self.mesh_buffer.clone()
    }

    pub fn get_face_buffer(&self) -> SubVec<ray_tracing::ty::Face>{
        self.faces_buffer.clone()
    }

    pub fn get_vertex_buffer(&self) -> SubVec<ray_tracing::ty::Vertex>{
        self.vertex_buffer.clone()
    }

    pub fn print_tree(&self){
        self.root.print_tree(0);
    }

}
