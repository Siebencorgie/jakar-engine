use core::resources::{texture, material, empty, mesh};
//use core::simple_scene_system::node;
use jakar_tree::*;
use core::next_tree::*;
use jakar_tree::node::{Attribute};

use core::resource_management::ManagerAndRenderInfo;
use core::ReturnBoundInfo;
use render::pipeline_builder;
use render::pipeline_manager;
use core::resources::material::{MaterialBuilder, MaterialFactors};
use core::resources::texture::TextureBuilder;
use core::resources::mesh::Vertex;

use render::render_passes::RenderPassConf;

use vulkano::sampler::Filter;
use vulkano::sampler::SamplerAddressMode;

use cgmath::*;

use gltf;
use gltf::*;

use image::ImageFormat;

use std::path::{Path, PathBuf};
use std::sync::{Arc, RwLock};
use std;

pub struct GltfImporter {
    manager: Arc<RwLock<ManagerAndRenderInfo>>,
    name: String,
    document: Document,
    buffers: Vec<buffer::Data>,
    images: Vec<image::Data>,
    tree: Option<JakarTree>,
    path_base: PathBuf,
}

impl GltfImporter{
    pub fn new(name: String, path: &Path, manager: Arc<RwLock<ManagerAndRenderInfo>>) -> Self{
        let (document, buffers, images) = import(path).expect("failed to load file");
        GltfImporter{
            manager: manager,
            name,
            document,
            buffers,
            images,
            tree: None,
            path_base: PathBuf::from(path.parent().expect("failed to determing gltf's parent path"))
        }
    }

    pub fn import(&mut self){

        //TODO only do if we have more than one scene in the file
        //We'll now build the tree recursivly by iterating through each scene and their nodes
        //Create the base tree
        let mut tree = tree::Tree::new(
            content::ContentType::Empty(empty::Empty::new(&self.name)),
            attributes::NodeAttributes::default()
        );

        for (idx, scene) in self.document.scenes().enumerate(){
            self.load_scene(
                scene,
                idx,
                &mut tree
            );
        }

        //Now overwrite selfs scene
        self.tree = Some(tree);

    }

    fn load_scene(&self, scene: Scene, idx: usize, current_tree: &mut JakarTree){
        //Now import the nodes recursively for each node
        //Add a node to the root with this scenes name
        let scene_name = if let Some(name) = scene.name(){
            name.to_string()
        }else{
            self.name.clone() + "_" + &idx.to_string()
        };

        let scene_node_name: String = current_tree.add_at_root(
            content::ContentType::Empty(empty::Empty::new(&scene_name)),
            None, None
        ).expect("failed to add new scene object to gltf scene tree");

        //now import the nodes
        for node in scene.nodes(){
            self.load_node(
                node,
                scene_node_name.clone(),
                None,
                current_tree
            );
        }
    }

    fn load_node(
        &self,
        node: Node,
        parent_name: String,
        parent_transform: Option<Decomposed<Vector3<f32>, Quaternion<f32>>>,
        current_tree: &mut JakarTree
    ){
        //Create our self
            //First find the transform of this node, then create the node and apply it
        let (translation, rotation, scale_gltf) = node.transform().decomposed();
        let mut disp = Vector3::new(translation[0], translation[1], translation[2]);
        let mut rot = Quaternion::new(rotation[3], rotation[0], rotation[1], rotation[2]);
        let mut scale = scale_gltf[0]; //TODO make scale non uniform
        //Check if we have to apply a parent transfrom
        if let Some(trans) = parent_transform{
            disp = disp + trans.disp;
            rot = rot * trans.rot;
            scale = scale * trans.scale;
        }

        //Since gltf uses a different format for the quaternion we have to set them a bit differen
        let mut new_attrib = attributes::NodeAttributes::default();
        new_attrib.transform.disp = disp;
        new_attrib.transform.rot = rot;
        new_attrib.transform.scale = scale;

        let node_name = if let Some(name) = node.name(){
            name.to_string()
        }else{
            parent_name.clone()
        };

        let final_node_name: String = current_tree.add(
            content::ContentType::Empty(empty::Empty::new(&node_name)),
            parent_name,
            Some(new_attrib.clone()),
            None
        ).expect("Failed to add node to tree");

        //Now load this nodes meshe
        if let Some(mesh) = node.mesh(){

            let mesh_name = if let Some(name) = mesh.name(){
                name.to_string()
            }else{
                (final_node_name.clone() + "_mesh").to_string()
            };

            //Add a node for this mesh
            let mesh_node_name = current_tree.add(
                content::ContentType::Empty(empty::Empty::new(&mesh_name)),
                final_node_name.clone(),
                Some(new_attrib.clone()),
                None
            ).expect("failed to add mesh collection node!");


            for (idx, primitve) in mesh.primitives().enumerate(){
                self.load_prim(
                    primitve,
                    mesh_node_name.clone(),
                    idx,
                    new_attrib.clone(),
                    current_tree
                );
            }
        }

        //Load an optional camera
        if let Some(cam) = node.camera(){
            self.load_camera(
                cam,
                final_node_name.clone(),
                current_tree
            );
        }

        //Finnaly load all children
        for child in node.children(){
            self.load_node(child, final_node_name.clone(), Some(new_attrib.transform), current_tree);
        }
    }

    fn load_prim(
        &self,
        mesh: Primitive,
        parent_name: String,
        index: usize,
        parent_attributes: attributes::NodeAttributes,
        current_tree: &mut JakarTree
    ){
        //Load its material then create the mesh from it
        let material = {
            if let Some(name) = mesh.material().name(){
                //Has a name, try to get it from the manager
                let material_search_result = {
                    let man_lck = self.manager.read().expect("failed to lock material manager");
                    let mat_man = man_lck.material_manager.clone();
                    let mut mat_man_lck = mat_man.read().expect("failed to lock material manager");
                    mat_man_lck.get_material_by_name(&name)
                };
                //Check if we got a valid material, otherwise load one
                if let Some(material) = material_search_result{
                    material
                }else{
                    self.load_material(mesh.material())
                }
            }else{
                //We don't have a name and therefore no way to detect the correct material
                //TODO we could actually have a look for the gltf id... but later :)
                self.load_material(mesh.material())
            }
        };

        let mesh_attributes = {
            let mut work_attrib = parent_attributes;
            match mesh.material().alpha_mode(){
                gltf::material::AlphaMode::Blend => work_attrib.is_transparent = true,
                _ => {},
            }

            if mesh.material().emissive_texture().is_some() || mesh.material().emissive_factor() != [0.0; 3]{
                work_attrib.is_emessive = true;
            }

            work_attrib
        };

        //Get all mesh data
        let (device, queue) = {
            let device = self.manager.read().expect("failed to lock manager").device.clone();
            let queue = self.manager.read().expect("failed to lock manager").queue.clone();
            (device, queue)
        };

        let primitive_name = parent_name.clone() + &index.to_string();


        //Create a reader which reads the
        let reader = mesh.reader(|buffer| self.to_buffer(&buffer));
        //Read indices
        let faces = if let Some(indice_iter) = reader.read_indices(){
            let mut indices = Vec::new();
            let u_iter = indice_iter.into_u32();
            for idx in u_iter{
                indices.push(idx);
            }
            indices
        }else{
            println!("WARNING: Gltf could not load indice iterator!", );
            Vec::new()
        };

        //Create the collection of vertices

        let positions_iter: Vec<[f32; 3]> = if let Some(pos_iter) = reader.read_positions(){
            pos_iter.collect()
        }else{
            Vec::new()
        };

        let normals_iter: Vec<[f32; 3]> = if let Some(norm_iter) = reader.read_normals(){
            norm_iter.collect()
        }else{
            Vec::new()
        };

        let tangents_iter: Vec<[f32; 4]> = if let Some(tan_iter) = reader.read_tangents(){
            tan_iter.collect()
        }else{
            Vec::new()
        };

        let colors_iter: Vec<[f32; 4]> = if let Some(col_iter) = reader.read_colors(0){
            col_iter.into_rgba_f32().collect()
        }else{
            Vec::new()
        };

        let texcord_iter: Vec<[f32; 2]> = if let Some(tex_iter) = reader.read_tex_coords(0){
            tex_iter.into_f32().collect()
        }else{
            Vec::new()
        };

        //small check, if we have no positions, there is no point in adding this mesh to the tree
        if positions_iter.len() <= 0{
            println!("WARNING: Positions buffer for primitive is empty!", );
            return;
        }

        //Now build an Vertex array by iterating the length of our position buffer
        //through them
        let mut vertex_buffer = Vec::new();
        for (idx, pos) in positions_iter.into_iter().enumerate(){
            vertex_buffer.push(
                //Add a vertex, replace missing information with default infos
                Vertex::new(
                    pos,
                    texcord_iter.get(idx).unwrap_or(&[0.0; 2]).clone(),
                    normals_iter.get(idx).unwrap_or(&[1.0; 3]).clone(),
                    tangents_iter.get(idx).unwrap_or(&[1.0; 4]).clone(),
                    colors_iter.get(idx).unwrap_or(&[1.0; 4]).clone()
                )
            );
        }

        //Set the index and vertex buffer now
        let mut new_mesh = mesh::Mesh::new(
            &primitive_name,
            device,
            material,
            vertex_buffer,
            faces,
            queue.transfer
        );
        //Create bounds vor the vertices
        new_mesh.rebuild_bound();

        //Add to manager
        //We finished the mesh, time to put it in an Arc<RwLock<mesh::Mesh>>
        let arc_mesh = Arc::new(RwLock::new(new_mesh));
        //Now copy it to the manager and push the other one to the return vector
        let mesh_manager = {
            let managers_lck = self.manager.read().expect("failed to lock managers struct");
            managers_lck.mesh_manager.clone()
        };
        {
            let mut mesh_manager_lck = mesh_manager.write().expect("failed to lock mesh manager in gltf loader");
            mesh_manager_lck.add_arc_mesh(arc_mesh.clone());
        }


        //push in node and set attributes
        //Now add the mesh to the tree
        //TODO use the actual name this mesh was created under and update it in the mesh manager
        let _final_mesh_name = current_tree.add(
            content::ContentType::Mesh(arc_mesh),
            parent_name,
            Some(mesh_attributes),
            None
        );

    }

    fn to_buffer(&self, buffer: &gltf::Buffer) -> Option<&[u8]>{
        //Try to read the buffer at index
        if buffer.index() >= self.buffers.len(){
            println!("WARNING: gltf import, tried to read outside of buffer", );
            None
        }else{
            Some(self.buffers[buffer.index()].0.as_slice())
        }
    }

    fn to_image(&self, view: &gltf::buffer::View) -> Option<&[u8]>{
        let data_buffer = self.to_buffer(&view.buffer());
        data_buffer.map(|data| {
            let begin = view.offset();
            let end = begin + view.length();
            &data[begin..end]
        })
    }

    fn load_material(&self, material: Material) -> Arc<RwLock<material::Material>>{
        let material_name = if let Some(name) = material.name(){
            name.to_string()
        }else{
            "unknown_material".to_string() //TODO make somehow nicer
        };

        //Now load all the textures, if needed
        let albedo_tex = if let Some(alb_tex) = material.pbr_metallic_roughness().base_color_texture(){
            Some(self.load_texture(alb_tex.texture()))
        }else{
            None
        };

        let metrough_tex = if let Some(metr_tex) = material.pbr_metallic_roughness().metallic_roughness_texture(){
            Some(self.load_texture(metr_tex.texture()))
        }else{
            None
        };

        let normal_tex = if let Some(nor) = material.normal_texture(){
            Some(self.load_texture(nor.texture()))
        }else{
            None
        };

        let occlusion_tex = if let Some(oc) = material.occlusion_texture(){
            Some(self.load_texture(oc.texture()))
        }else{
            None
        };

        let emissive_tex = if let Some(em) = material.emissive_texture(){
            Some(self.load_texture(em.texture()))
        }else{
            None
        };

        let fall_back = {
            let mans = self.manager.read().expect("failed to lock manager");
            let mut tex_man = mans.texture_manager.read().expect("faild to lock tex manager");
            let none_tex = tex_man.get_none();
            none_tex
        };


        //Build the material based on the settings
        let mut material_builder = MaterialBuilder::new(
            albedo_tex,
            normal_tex,
            metrough_tex,
            occlusion_tex,
            emissive_tex,
            fall_back
        );

        //now set the factors
        let material_factors = MaterialFactors::new(
            material.pbr_metallic_roughness().base_color_factor(),
            1.0, // normal_factor
            material.emissive_factor(),
            1600.0, //max_emission currently set to something really high
            material.pbr_metallic_roughness().metallic_factor(),
            material.pbr_metallic_roughness().roughness_factor(),
            1.0, //occlusion_factor
            material.alpha_cutoff(),
        );

        //Build material
        material_builder = material_builder.with_factors(material_factors);

        //Now configure a pipeline

        //To decide the pipeline of this material we need to know which attributes it has, we'll read
        // blending mode and culling from the material struct of the gltf model as well as the poly
        // mode from the parent polygone
        let blending_mode = {
            match material.alpha_mode(){
                gltf::material::AlphaMode::Opaque =>{
                    //println!("RENDING PASS THROUGH! ======================================================", );
                    pipeline_builder::BlendTypes::BlendPassThrough
                },
                gltf::material::AlphaMode::Mask =>{
                    //println!("RENDING ALPHA BLENDING! ======================================================", );
                    //Also setup masking for the flags for early out in the fragment shader
                    material_builder.mat_is_masked();
                    pipeline_builder::BlendTypes::BlendPassThrough
                },
                gltf::material::AlphaMode::Blend =>{
                    //println!("RENDING ALPHA BLENDING! ======================================================", );
                    pipeline_builder::BlendTypes::BlendAlphaBlending
                },

            }
        };

        let cull_mode = {
            if material.double_sided(){
                //println!("RENDING DOUBLE SIDED! ======================================================", );
                if material_builder.is_masked(){
                    pipeline_builder::CullMode::Disabled
                }else{
                    pipeline_builder::CullMode::Back //TODO I have to implement order independent transparency for this to work in complex models
                }
            }else{
                //println!("RENDING SINGLE SIDED! ======================================================", );
                pipeline_builder::CullMode::Back
            }
        };

        //now create the requirements based on it
        let requirements = pipeline_manager::PipelineRequirements{
            blend_type: blending_mode,
            culling: cull_mode,
            render_pass: RenderPassConf::ObjectPass,
            shader_set: "Pbr".to_string(),
        };


        //Get the incredienses for building a material
        let (pipeline, uniform_manager, device) = {
            //get the device we are on
            let device = {
                let managers_lck = self.manager.read().expect("failed to lock managers struct");
                managers_lck.device.clone()
            };

            //get the manager
            let pipeline_manager = {
                let managers_lck = self.manager.read().expect("failed to lock managers struct");
                managers_lck.pipeline_manager.clone()
            };

            //Get the pipeline based on the needs of this material
            let mut pipeline_manager_lck = pipeline_manager.write().expect("failed to lock pipe manager");
            //Build the pipeline by the requirements
            let pipeline = (*pipeline_manager_lck).get_pipeline_by_requirements(
                requirements
            );


            let uniform_manager = self.manager.read()
            .expect("failed to lock managers struct").uniform_manager.clone();


            (pipeline, uniform_manager, device)
        };

        //build the final material
        let final_material = material_builder.build(&material_name, pipeline, uniform_manager, device);
        let material_manager = self.manager.read().expect("failed to read managers").material_manager.clone();

        //now add a copy to the manager and return the name
        let mut material_manager_lck = material_manager.write().expect("failed to lock material manager");
        //Add it and return its
        //println!("Finished loading material with name: {}", material_name);
        let name = material_manager_lck.add_material(final_material);

        //Now return the new material
        material_manager_lck.get_material(&name)


    }

    fn load_texture(&self, texture: gltf::texture::Texture) -> Arc<texture::Texture>{

        let (device, queue, texture_manager) = {
            let manager = self.manager.write().expect("failed to lock manager");
            (manager.device.clone(), manager.queue.clone(), manager.texture_manager.clone())
        };

        //Check if we already loaded this texture, if so, do not reimport
        {
            if let Some(tex_name) = texture.name(){
                let tex_man = texture_manager.read().expect("failed to read texture manager");
                if let Some(texture) = tex_man.get_texture(&tex_name){
                    println!("Returning already found texture!", );
                    return texture;
                }
            }

        }

        //Match how we start the texture build based on the source type
        let mut texture_builder = match texture.source().source(){
            gltf::image::Source::Uri{uri, mime_type} => {

                let final_uri = self.path_base.join(uri);
                println!("Loading image: {:?}", final_uri);
                TextureBuilder::from_image(
                    final_uri,
                    device.clone(),
                    queue.clone()
                )
            },
            gltf::image::Source::View{view, mime_type} => {
                println!("Loading image from buffer with mimetype: {}!", mime_type);
                TextureBuilder::from_data(
                    (
                        self.to_image(&view).expect("failed to load image").to_vec(),
                        format_from_str(mime_type)
                    ), //TODO verfy that its the right buffer
                    device.clone(),
                    queue.clone()
                )
            }
        };

        let image_sampler = texture.sampler();

        let mag_filter = if let Some(mag_filter) = image_sampler.mag_filter(){
            match mag_filter{
                gltf::texture::MagFilter::Nearest => Filter::Nearest,
                gltf::texture::MagFilter::Linear => Filter::Linear,
            }
        }else{
            Filter::Linear
        };

        let min_filter = if let Some(min_filter) = image_sampler.min_filter(){
            match min_filter{
                gltf::texture::MinFilter::Nearest => Filter::Nearest,
                gltf::texture::MinFilter::Linear => Filter::Linear,
                _ => {
                    println!("WARNING: Filter is set to linear", );
                    Filter::Linear
                }
            }
        }else{
            Filter::Linear
        };

        texture_builder = texture_builder.with_sampling_filter(mag_filter, min_filter);

        let wraping_u = match image_sampler.wrap_s(){
            gltf::texture::WrappingMode::ClampToEdge => SamplerAddressMode::ClampToEdge,
            gltf::texture::WrappingMode::MirroredRepeat => SamplerAddressMode::MirroredRepeat,
            gltf::texture::WrappingMode::Repeat => SamplerAddressMode::Repeat,
        };

        let wraping_v = match image_sampler.wrap_t(){
            gltf::texture::WrappingMode::ClampToEdge => SamplerAddressMode::ClampToEdge,
            gltf::texture::WrappingMode::MirroredRepeat => SamplerAddressMode::MirroredRepeat,
            gltf::texture::WrappingMode::Repeat => SamplerAddressMode::Repeat,
        };

        //TODO find a nice way to set wraping in z direction if needed
        texture_builder = texture_builder.with_tiling_mode(wraping_u, wraping_v, wraping_v);

        //Set the mip mapping

        //Now build the texture with its name
        let texture_name = if let Some(name) = texture.name(){
            name.to_string()
        }else{
            "texture".to_string()
        };

        let valid_texture_name = texture_manager.read().expect("Failed to read Texture manager").get_valid_name(texture_name);
        let final_texture = texture_builder.build_with_name(&valid_texture_name);
        //Add it to the manager
        {
            let mut man_lck = texture_manager.write().expect("failed to lock texture manager");
            if let Err(er) = man_lck.add_texture(final_texture.clone()){
                println!("Warning: Failed to add texture to manager while importing: {}", er);
            }
        }

        //now return the final texture
        final_texture
    }

    fn load_camera(&self, camera: Camera, parent_name: String, current_tree: &mut JakarTree){

    }

    ///Returns the new scene tree, blocks as long as there is none
    pub fn get_scene<'a>(self) -> std::result::Result<JakarTree, &'a str>{
        //TODO load a debug object if something failed
        if let Some(tree) = self.tree{
            Ok(tree)
        }else{
            Err("failed to load tree")
        }
    }
}

///Creates and image::ImageFormat from a mime_type string
pub fn format_from_str<'a>(mime_type: &'a str) -> ImageFormat{
    let mime_type_string = mime_type.to_string();

    if mime_type_string.contains("png"){
         return ImageFormat::PNG;
    }

    if mime_type_string.contains("jpeg"){
         return ImageFormat::JPEG;
    }

    if mime_type_string.contains("gif"){
         return ImageFormat::GIF;
    }

    if mime_type_string.contains("webp"){
         return ImageFormat::WEBP;
    }

    if mime_type_string.contains("pnm"){
         return ImageFormat::PNM;
    }

    if mime_type_string.contains("tiff"){
         return ImageFormat::TIFF;
    }

    if mime_type_string.contains("tga"){
         return ImageFormat::TGA;
    }

    if mime_type_string.contains("bmp"){
         return ImageFormat::BMP;
    }

    if mime_type_string.contains("ico"){
         return ImageFormat::ICO;
    }

    if mime_type_string.contains("hdr"){
         return ImageFormat::HDR;
    }

    ImageFormat::PNG
}
