use std::sync::Arc;

///Collection of useful functions on a generic vector.
pub trait VecTools<T>{
    //Copys a slice to the end of an vec
    fn copy_into_vec(&mut self, other: &Vec<T>);
}

impl<T: Copy> VecTools<T> for Vec<T>{
    ///Unsafe: This function usually works but is not tested enought. But it speeds up appending by a factor
    /// of around 3 compared to the usualy `vec.append()`.
    fn copy_into_vec(&mut self, other: &Vec<T>){

        let self_length = self.len();

        unsafe {
            self.reserve(other.len());
            self.set_len(self_length + other.len());
            self[self_length .. self_length + other.len()].copy_from_slice(&other);
        }
    }
}

///Can chain several heap allocated pointers to vectors and iterate over them.
///Warning: Currently only from type Arv<Vec<T>>
#[derive(Clone)]
pub struct SubVec<T> {
    //The items in this collection
    inner: Vec<Arc<Vec<T>>>,
    //The count used to iterate over all elements if turned into an iterator
    count: usize,
    //The length of this iterator
    len: usize,
    //Points to the index of the last sub node we iterated over
    last_sub: usize,
    //points to the index within the last sub node
    sub_idx: usize,
}

impl<T> SubVec<T>{
    pub fn new() -> Self{
        SubVec{
            inner: Vec::new(),
            count: 0,
            len: 0,
            last_sub: 0,
            sub_idx: 0,
        }
    }
    pub fn push(&mut self, new: Arc<Vec<T>>){
        self.len += new.len();
        self.inner.push(new);
    }
}

impl<T: Copy> Iterator for SubVec<T>{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item>{
        if self.count < self.len{
            //Check if we can get a new index within the last node
            if self.sub_idx < self.inner[self.last_sub].len(){
                //Advance for next loop
                self.sub_idx += 1;
                //Return the last index
                self.count += 1;
                return Some(self.inner[self.last_sub][self.sub_idx - 1])
            }else{
                //We have to change to a new node
                //first check if we can advance otherwise we can break
                if (self.last_sub + 1) >= self.inner.len(){
                    self.count += 1;
                    return None;
                }

                //we advance to the next node
                self.last_sub += 1;
                self.sub_idx = 0; //And reset the inner index
                //Last but not least check that we can access the current idx
                if self.inner[self.last_sub].len() <= 0{
                    self.count += 1;
                    return None;
                }else{
                    self.sub_idx += 1;
                    self.count += 1;
                    return Some(self.inner[self.last_sub][self.sub_idx - 1]);
                }
            }
        }else{
            //No elements left
            self.count += 1;
            None
        }
    }
}

impl<T: Copy> ExactSizeIterator for SubVec<T> {
    // We can easily calculate the remaining number of iterations.
    fn len(&self) -> usize {
        self.len - self.count
    }
}
