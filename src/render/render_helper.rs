use core::resources::mesh;
use core::resources::camera;
use core::resources::camera::Camera;
use jakar_tree::*;
use jakar_threadpool::*;
use core::next_tree::*;
use render::pipeline_manager;
use render::pipeline_builder;
use render::uniform_manager;
use render::render_passes::RenderPassConf;

use std::sync::{Arc, RwLock};
use std::sync::mpsc;
use std::collections::BTreeMap;

use cgmath::*;
use collision::*;

use vulkano;
use vulkano::descriptor::descriptor_set::PersistentDescriptorSet;
use vulkano::command_buffer::AutoCommandBufferBuilder;

///Returns a thread handle which, at some point returns a ordered vector of the provided
/// `meshes` based on their distance to the `camera` (the furthest away is the first mesh, the neares is the last).
pub fn order_by_distance(
    mehes: Vec<JakarNode>,
    camera: &camera::DefaultCamera,
    thread_pool: &mut ThreadPool,
) -> mpsc::Receiver<Vec<node::Node<content::ContentType, jobs::SceneJobs, attributes::NodeAttributes>>>{
    //Create the pipe
    let (sender, reciver) = mpsc::channel();
    //extract the position of the camera needed for the calculation
    let camera_location = camera.get_position();

    thread_pool.execute(move ||{

        //Silly ordering
        let mut ordered_meshes: BTreeMap<i64, node::Node<content::ContentType, jobs::SceneJobs, attributes::NodeAttributes>> = BTreeMap::new();

        for mesh in mehes.iter(){

            use cgmath::InnerSpace;

            let mesh_location = mesh.get_attrib().get_transform().disp;

            //get distance between camera and position
            let distance = mesh_location - camera_location;
            //now transform to an int and multiply by 10_000 to have some comma for better sorting
            let i_distance = (distance.magnitude().abs() * 10_000.0) as i64;

            //now add the mesh to the map based on it
            ordered_meshes.insert(i_distance, mesh.clone());

        }
        //Silly ordering end ==================================================================

        //now reorder the meshes reversed into a vec and send them to the render thread
        let mut return_vector = Vec::new();
        for (_, mesh) in ordered_meshes.into_iter().rev(){
            return_vector.push(mesh);
        }
        match sender.send(return_vector){
            Ok(_) => {},
            Err(er) => panic!("failed to send ordered meshes! {:?}", er)
        }

    });

    //return the reciver for further working on the renderer
    reciver
}


//Function used to draw a single bound with a specified color.
pub fn draw_bound(
    command_buffer: AutoCommandBufferBuilder,
    pipeline_manager: Arc<RwLock<pipeline_manager::PipelineManager>>,
    aabb: Aabb3<f32>,
    device: Arc<vulkano::device::Device>,
    uniform_manager: Arc<RwLock<uniform_manager::UniformManager>>,
    dynamic_state: &vulkano::command_buffer::DynamicState,
    color: [f32; 4]
) -> AutoCommandBufferBuilder{

        //Create a vertex buffer for the bound
        let mut pipeline_needed = pipeline_builder::PipelineConfig::default();
        //Setup the wireframe shader and the topology type
        pipeline_needed.topology_type = vulkano::pipeline::input_assembly::PrimitiveTopology::LineList;
        pipeline_needed.shader_set = "Wireframe".to_string();
        pipeline_needed.render_pass = RenderPassConf::ObjectPass;

        let mut pipeline_lck = pipeline_manager.write().expect("failed to lock pipeline manager");
        let pipeline = pipeline_lck.get_pipeline_by_config(pipeline_needed);

        //Find the mins and max, and create vertex buffer for them
        let value_vertices = create_vertex_buffer_for_bound(aabb.min, aabb.max, color);

        let indices: Vec<u32> = vec![
            0,1, //lower quad
            1,2,
            2,3,
            3,0,
            0,5,//columns
            1,6,
            2,7,
            3,4,
            5,6,//upper quad
            6,7,
            7,4,
            4,5
        ];

        //Now make an indice buffer
        let index_buffer = vulkano::buffer::cpu_access::CpuAccessibleBuffer
            ::from_iter(device.clone(), vulkano::buffer::BufferUsage::index_buffer(), indices.iter().cloned())
            .expect("failed to create index buffer 02");

        //and an vertex buffer
        let value_vertex_buffer = vulkano::buffer::cpu_access::CpuAccessibleBuffer
                                    ::from_iter(
                                        device.clone(),
                                        vulkano::buffer::BufferUsage::vertex_buffer(),
                                        value_vertices.iter().cloned())
                                    .expect("failed to create buffer");

        //We also have to create a descriptor set for the MVP stuff
        let mvp_data = uniform_manager.write().expect("failed to lock uniform manager")
        .get_subbuffer_data(Matrix4::identity()); //We transformed the points our selfs
                                                                  //Thats why we use no model matrix
        //now create the set for the value
        let attachments_ds_value = PersistentDescriptorSet::start(pipeline.get_pipeline_ref(), 0)
            .add_buffer(mvp_data.clone())
            .expect("failed to add depth image")
            .build()
            .expect("failed to build postprogress cb");

        //draw the value bound
        let new_cb = command_buffer.draw_indexed(
            pipeline.get_pipeline_ref(),
            dynamic_state,
            vec![value_vertex_buffer],
            index_buffer.clone(),
            attachments_ds_value,
            () //also no constants
        ).expect("failed to draw bounds!");


        new_cb
}

///Function used to draw the bounds of a node within the scene tree.
pub fn add_bound_draw(
    command_buffer: AutoCommandBufferBuilder,
    pipeline_manager: Arc<RwLock<pipeline_manager::PipelineManager>>,
    object_node: &JakarNode,
    device: Arc<vulkano::device::Device>,
    uniform_manager: Arc<RwLock<uniform_manager::UniformManager>>,
    dynamic_state: &vulkano::command_buffer::DynamicState,
) -> AutoCommandBufferBuilder{

    let with_value = draw_bound(
        command_buffer,
        pipeline_manager.clone(),
        object_node.get_attrib().get_value_bound().clone(),
        device.clone(),
        uniform_manager.clone(),
        dynamic_state,
        [0.7, 0.5, 0.0, 1.0]
    );

    //now also draw the bound of this node
    draw_bound(
        with_value,
        pipeline_manager,
        object_node.get_attrib().get_bound().clone(),
        device,
        uniform_manager,
        dynamic_state,
        [0.0, 0.25, 0.5, 1.0]
    )

}

///A helper function to create the vertex_buffer for a bound
fn create_vertex_buffer_for_bound(min: Point3<f32>, max: Point3<f32>, color: [f32; 4]) -> Vec<mesh::Vertex> {
    let vertices = vec![
        //Point 1
        mesh::Vertex::new(                            //one could configure the bound color at this point
            [min.x, min.y, min.z], [0.0; 2], [0.0; 3], [0.0; 4], color
        ),
        //Point 2
        mesh::Vertex::new(
            [max.x, min.y, min.z] , [0.0; 2], [0.0; 3], [0.0; 4], color
        ),
        //Point 3
        mesh::Vertex::new(                            //one could configure the bound color at this point
            [max.x, max.y, min.z] , [0.0; 2], [0.0; 3], [0.0; 4], color
        ),
        //Point 4
        mesh::Vertex::new(
            [min.x, max.y, min.z] , [0.0; 2], [0.0; 3], [0.0; 4], color
        ),
        //END LOWER
        //Point 5
        mesh::Vertex::new(                            //one could configure the bound color at this point
            [min.x, max.y, max.z] , [0.0; 2], [0.0; 3], [0.0; 4], color
        ),
        //Point 6
        mesh::Vertex::new(
            [min.x, min.y, max.z] , [0.0; 2], [0.0; 3], [0.0; 4], color
        ),
        //Point 7
        mesh::Vertex::new(                            //one could configure the bound color at this point
            [max.x, min.y, max.z] , [0.0; 2], [0.0; 3], [0.0; 4], color
        ),
        //Point 8
        mesh::Vertex::new(
            [max.x, max.y, max.z] , [0.0; 2], [0.0; 3], [0.0; 4], color
        ),

    ];

    vertices
}
