use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::device::QueuesIter;
use render::pipeline_manager;
use render::uniform_manager;
use core::resource_management::asset_manager;
use core::resources::camera::Camera;
use render::window;
use render::window::Window;
use render::frame_system;
use render::post_process;
use render::light_system;
use render::render_passes::RenderPasses;
use render::object_rendering::ObjectSystem;
use render::raytracing::RayTracing;

use core::engine_settings;
//use core::simple_scene_system::node_helper;
use tools::engine_state_machine::RenderState;
use tools::math::time_tools::*;

use jakar_threadpool::*;

use winit;
use vulkano;
use vulkano::swapchain::{SwapchainAcquireFuture, AcquireError, SwapchainCreationError, PresentFuture};
use vulkano::sync::{GpuFuture, FenceSignalFuture};
use vulkano::command_buffer::{CommandBufferExecFuture, AutoCommandBuffer, CommandBuffer};
use vulkano::device::{Queue, Device};
use vulkano::instance::QueueFamily;

use std::sync::{Arc,RwLock};
use std::time::{Instant, Duration};
use std::mem;


///manages some some debug information
pub struct RenderDebug {
    last_sec_start: Instant,
    current_counter: u32,
    avg_mesh_render_time: f32,
    avg_set_time: f32,
    avg_draw_command_time: f32,
    avg_node_getting: f32,
    last_draw_calls: u32,

    mesh_capture_start: Instant,
    mesh_set_cap_start: Instant,
    mesh_draw_start: Instant,
    mesh_node_getting: Instant,
}

impl RenderDebug{
    pub fn new() -> Self{
        RenderDebug{
            last_sec_start: Instant::now(),
            current_counter: 0,
            avg_mesh_render_time: 1.0 / 1_000.0,
            avg_set_time: 1.0 / 1_000.0,
            avg_draw_command_time: 1.0 / 1_000.0,
            avg_node_getting: 1.0 / 1_000.0,
            last_draw_calls: 0,
            mesh_capture_start: Instant::now(),
            mesh_set_cap_start: Instant::now(),
            mesh_draw_start: Instant::now(),
            mesh_node_getting: Instant::now(),
        }
    }
    pub fn update(&mut self){
        if self.last_sec_start.elapsed().as_secs() > 0{
            println!("FPS: {} \t avg mesh timing: {}ms", self.current_counter, self.avg_mesh_render_time * 1000.0);
            self.last_sec_start = Instant::now();
            self.current_counter = 1;
        }else{
            self.current_counter += 1;
        }
    }

    pub fn set_draw_calls(&mut self, draw_calls: u32){
        self.last_draw_calls = draw_calls;
    }

    pub fn end_mesh_capture(&mut self){
        let dur = self.mesh_capture_start.elapsed();
        self.avg_mesh_render_time += dur_as_f32(dur);
        self.avg_mesh_render_time /= 2.0;
    }

    //starts the capture of a mesh rendering time, gets reset when update_avr_mesh is called
    pub fn start_mesh_capture(&mut self){
        self.mesh_capture_start = Instant::now();
    }

    pub fn start_set_gen(&mut self){
        self.mesh_set_cap_start = Instant::now();
    }

    pub fn end_mesh_set(&mut self){
        let dur = self.mesh_set_cap_start.elapsed();
        self.avg_set_time += dur_as_f32(dur);
        self.avg_set_time /= 2.0;
    }

    pub fn start_draw_cmd(&mut self){
        self.mesh_draw_start = Instant::now();
    }

    pub fn end_draw_cmd(&mut self){
        let dur = self.mesh_draw_start.elapsed();
        self.avg_draw_command_time += dur_as_f32(dur);
        self.avg_draw_command_time /= 2.0;
    }
    pub fn start_node_getting(&mut self){
        self.mesh_node_getting = Instant::now();
    }

    pub fn end_node_getting(&mut self){
        let dur = self.mesh_node_getting.elapsed();
        self.avg_node_getting += dur_as_f32(dur);
        self.avg_node_getting /= 2.0;
    }

    pub fn print_stat(&self){
        println!("RenderStats=================", );
        println!("Average mesh draw time: {}ms", self.avg_mesh_render_time * 1000.0);
        println!("Average mesh desc-set time: {}ms", self.avg_set_time * 1000.0);
        println!("Average mesh draw command time: {}ms", self.avg_draw_command_time * 1000.0);
        println!("Average node getting time: {}ms", self.avg_node_getting * 1000.0);
        println!("-----At: {} draw calls ---", self.last_draw_calls);
        println!("============================", );

    }
}


///Used tp build a render instance
pub trait BuildRender {
    ///Build a renderer based on settings and a window which will recive the iamges
    fn build(
        self,
        window: Window,
    ) -> Result<Renderer, String>;
}

///Collects all queues used on the gpu.
///The following ist guranteed:
/// - graphics: Can do graphics operation
/// - compute: Can do compute operation
/// - transfer: Can upload / Download to the gpu, but no operations.
/// However, all of thoose queues can probably do more, depending on the gpu type.
#[derive(Clone)]
pub struct JkQueues {
    pub graphics: Arc<Queue>,
    pub compute: Arc<Queue>,
    pub transfer: Arc<Queue>,
}

impl JkQueues{
    pub fn new(queues: QueuesIter) -> Self{
        let mut graphic_queue = None;
        let mut compute_queue = None;
        let mut transfer_queue = None;

        //Now check the queues which where created
        for q in queues{
            if q.family().supports_graphics() && graphic_queue.is_none(){
                graphic_queue = Some(q);
                continue;
            }

            if q.family().supports_compute() && compute_queue.is_none(){
                compute_queue = Some(q);
                continue;
            }

            if !q.family().supports_compute() && !q.family().supports_graphics() && transfer_queue.is_none(){
                transfer_queue = Some(q);
                continue;
            }
        }

        //Check the queues
        if graphic_queue.is_none(){
            panic!("No graphics queue found");
        }

        if compute_queue.is_none(){
            println!("WARNING: No compute queue found, using graphics queue", );
            compute_queue = graphic_queue.clone();
        }

        //Currently always using compute queue since multi queue sync is not implemented for vulkano atm.
        transfer_queue = compute_queue.clone();

        if transfer_queue.is_none(){
            println!("WARNING: No transfer queue found, using compute", );
            transfer_queue = compute_queue.clone();
        }

        JkQueues{
            graphics: graphic_queue.expect("Failed to find graphics queue"),
            compute: compute_queue.expect("Failed to find compute queue"),
            transfer: transfer_queue.expect("Failed to find transfer queue"),
        }
    }

    pub fn get_families(&self) -> Vec<QueueFamily<'_>>{
            vec![
                self.graphics.family(),
                self.compute.family(),
                self.transfer.family()
            ]

    }
}

///Transports the graphics and compute command buffer of one frame, as well as the future, which handels
/// the synchronisation between both
pub struct JkCommandBuff {
    //represents the current graphics operation which can be chained together.
    graphics_cb: Option<AutoCommandBufferBuilder>,
    compute_cb: Option<AutoCommandBufferBuilder>,
    future: Option<Box<GpuFuture + Send + Sync>>,
    queues: JkQueues,
    device: Arc<Device>
}

impl JkCommandBuff{
    pub fn new(pending_operations: Option<Box<GpuFuture + Send + Sync>>, device: Arc<Device>, queues: JkQueues) -> Self{
        JkCommandBuff{
            graphics_cb: None,
            compute_cb: None,
            future: if let Some(future) = pending_operations{
                Some(future)
            }else{
                Some(Box::new(vulkano::sync::now(device.clone())))
            },
            device: device,
            queues: queues,
        }
    }

    pub fn set_graphics_buffer(&mut self, new: AutoCommandBufferBuilder){
        self.graphics_cb = Some(new);
    }

    pub fn get_graphics_buffer(&mut self) -> AutoCommandBufferBuilder{

        //Check if we need to create a new one
        if self.graphics_cb.is_none(){
            self.graphics_cb = Some(vulkano::command_buffer::AutoCommandBufferBuilder::new(
                self.device.clone(),
                self.queues.graphics.family()
            ).expect("failed to create Graphics command buffer!"));
        }

        if let Some(current_cb) = self.graphics_cb.take(){
            return current_cb;
        }

        println!("WARNING Could not find Graphics command buffer!", );
        panic!("");
    }

    pub fn set_compute_buffer(&mut self, new: AutoCommandBufferBuilder){
        self.compute_cb = Some(new);
    }

    pub fn get_compute_buffer(&mut self) -> AutoCommandBufferBuilder{

        //Check if we need to create a new one
        if self.compute_cb.is_none(){
            self.compute_cb = Some(vulkano::command_buffer::AutoCommandBufferBuilder::new(
                self.device.clone(),
                self.queues.compute.family()
            ).expect("failed to create Compute command buffer!"));
        }

        if let Some(current_cb) = self.compute_cb.take(){
            return current_cb;
        }

        println!("WARNING Could not find Compute command buffer!", );
        panic!("");
    }

    ///Executes both, graphics and compute commands currently chained up. Then signals semaphores.
    ///Use this if you need the outcome of a different queue. For instance, if the compute queue created
    /// A buffer which is needed on the graphics queue.
    pub fn sync_queues(&mut self){

        let mut current_future: Box<GpuFuture + Send + Sync> = if let Some(fut) = self.future.take(){
            fut
        }else{
            Box::new(vulkano::sync::now(self.device.clone()))
        };

        current_future = Box::new(current_future.then_signal_semaphore());

        //Execute graphics cb
        current_future = if let Some(cb) = self.graphics_cb.take(){
            let command_buffer = cb.build().expect("failed to build command buffer");
            //Submit graphics cb.
            Box::new(
                current_future
                .then_execute(self.queues.graphics.clone(), command_buffer)
                .expect("failed to execute graphics command buffer")
                .then_signal_semaphore()
            )
        }else{
            current_future
        };
        //Execute compute_cb
        current_future = if let Some(cb) = self.compute_cb.take(){
            let command_buffer = cb.build().expect("failed to build command buffer");
            //Submit compute cb.
            Box::new(
                current_future
                .then_execute(self.queues.compute.clone(), command_buffer)
                .expect("failed to execute compute command buffer")
                .then_signal_semaphore()
            )
        }else{
            current_future
        };

        //Now join both futures to represnt the point where graphics and compute command buffers are
        //Done, then signal semaphores to sync data between both queues

        //Finally signal semaphores for queue sync
        self.future = Some(Box::new(
            current_future
        ));

    }

    //Joins the current future with another one, WARNING, potentually unsafe, make sure the future doesnt
    //depend on anything which is not submitted yet.
    pub fn join(&mut self, other: Box<GpuFuture + Send + Sync>){
        let future = if let Some(fut) = self.future.take(){
            fut
        }else{
            //Dont have to join
            self.future = Some(other);
            return;
        };

        self.future = Some(Box::new(future.join(other)));
    }

    ///Finishes this Command Buffer by submitting remaining graphics cb and then compute cb.
    ///Returns the future representing the ending of both
    pub fn finish(mut self) -> Box<GpuFuture + Send + Sync>{
        self.sync_queues();

        self.future.expect("Could not find CommandBuffer Future")
    }
}


///The main renderer. Should be created through a RenderBuilder
pub struct Renderer {
    ///Holds the renderers pipeline_manager
    pipeline_manager: Arc<RwLock<pipeline_manager::PipelineManager>>,


    //window: vulkano_win::Window,
    window: window::Window,
    device: Arc<Device>,
    queue: JkQueues,
    swapchain: Arc<vulkano::swapchain::Swapchain<winit::Window>>,
    images: Vec<Arc<vulkano::image::SwapchainImage<winit::Window>>>,

    frame_system: frame_system::FrameSystem,
    object_rendering: ObjectSystem,
    light_system: light_system::LightSystem,
    render_passes: Arc<RwLock<RenderPasses>>,
    ray_tracing: RayTracing,

    ///The post progresser
    post_process: post_process::PostProcess,

    //Is true if we need to recreate the swap chain
    recreate_swapchain: bool,

    engine_settings: Arc<RwLock<engine_settings::EngineSettings>>,
    uniform_manager: Arc<RwLock<uniform_manager::UniformManager>>,

    state: Arc<RwLock<RenderState>>,
    last_frame_end: Option<Arc<FenceSignalFuture<PresentFuture<CommandBufferExecFuture<Box<vulkano::sync::GpuFuture + Send + Sync>, AutoCommandBuffer>, winit::Window>>>>,

    ///A Threadpool used for all rendering operation threading like draw_call batching
    render_thread_pool: ThreadPool,

    debug_info: RenderDebug,
}

impl Renderer {
    ///Creates a new renderer from all the systems. However, you should only use the builder to create
    /// a renderer.
    pub fn create_for_builder(
        pipeline_manager: Arc<RwLock<pipeline_manager::PipelineManager>>,
        window: window::Window,
        device: Arc<Device>,
        queue: JkQueues,
        swapchain: Arc<vulkano::swapchain::Swapchain<winit::Window>>,
        images: Vec<Arc<vulkano::image::SwapchainImage<winit::Window>>>,

        //the used frame system
        frame_system: frame_system::FrameSystem,

        render_passes: Arc<RwLock<RenderPasses>>,
        object_rendering: ObjectSystem,
        light_system: light_system::LightSystem,
        ray_tracing: RayTracing,

        post_process: post_process::PostProcess,

        recreate_swapchain: bool,
        engine_settings: Arc<RwLock<engine_settings::EngineSettings>>,
        uniform_manager: Arc<RwLock<uniform_manager::UniformManager>>,

        render_thread_pool: ThreadPool,

        state: Arc<RwLock<RenderState>>,
    ) -> Renderer{
        Renderer{
            pipeline_manager: pipeline_manager,
            window: window,
            device: device,
            queue: queue,
            swapchain: swapchain,
            images: images,
            //Helper systems, the frame system handles... well a frame, the post progress writes the
            //static post_process pass.AcquireError
            frame_system: frame_system,
            object_rendering: object_rendering,
            render_passes: render_passes,
            light_system: light_system,
            ray_tracing: ray_tracing,
            post_process: post_process,

            recreate_swapchain: recreate_swapchain,
            engine_settings: engine_settings,
            uniform_manager: uniform_manager,
            state: state,
            last_frame_end: None,

            render_thread_pool,

            debug_info: RenderDebug::new(),
        }
    }

    ///Recreates swapchain for the window size.
    ///Returns true if successfully recreated chain
    pub fn recreate_swapchain(&mut self) -> bool{
        //get new dimmensions etc

        //Update the widow dimensions in scope to prevent locking
        let new_dimensions = {
            let mut engine_settings_lck = self.engine_settings
            .write()
            .expect("Faield to lock settings");

            let c_d = self.window.get_current_extend();
            let (new_width, new_height) = (c_d[0], c_d[1]);
            engine_settings_lck.set_dimensions(new_width, new_height);
            engine_settings_lck.get_dimensions()
        };

        let (new_swapchain, new_images) =
        match self.swapchain.recreate_with_dimension(new_dimensions) {
            Ok(r) => r,
            // This error tends to happen when the user is manually resizing the window.
            // Simply restarting the loop is the easiest way to fix this issue.
            Err(SwapchainCreationError::UnsupportedDimensions) => {
                return false;
            },
            Err(err) => panic!("{:?}", err)
        };

        //Now repace
        mem::replace(&mut self.swapchain, new_swapchain);
        mem::replace(&mut self.images, new_images);

        //with the new dimensions set in the setting, recreate the images of the frame system as well
        self.frame_system.recreate_attachments();

        //Now when can mark the swapchain as "fine" again
        self.recreate_swapchain = false;
        true
    }

    ///Returns the image if the image state is outdated
    ///Panics if another error occures while pulling a new image
    pub fn check_image_state(&self) -> Result<(usize, SwapchainAcquireFuture<winit::Window>), AcquireError>{

        match vulkano::swapchain::acquire_next_image(self.swapchain.clone(), None) {
            Ok(r) => {
                return Ok(r);
            },
            Err(vulkano::swapchain::AcquireError::OutOfDate) => {
                return Err(vulkano::swapchain::AcquireError::OutOfDate);
            },
            Err(err) => panic!("{:?}", err)
        };
    }

    ///checks the pipeline. If not up to date (return is AcquireError), recreates it.
    fn check_swapchain(&mut self) -> Result<(usize, SwapchainAcquireFuture<winit::Window>), AcquireError>{
        //If found out in last frame that images are out of sync, generate new ones
        if self.recreate_swapchain{
            if !self.recreate_swapchain(){
                //If we got the UnsupportedDimensions Error (and therefor returned false)
                //Abord the frame
                println!("failed to recreate new swapchain", );
                return Err(AcquireError::SurfaceLost);
            }
        }
        //Try to get a new image
        //If not possible becuase outdated (result is Err)
        //then return (abort frame)
        //and recreate swapchain
        match self.check_image_state(){
            Ok(r) => {
                return Ok(r)
            },
            Err(er) => {
                self.recreate_swapchain = true;
                return Err(er);
            },
        };
    }

    ///Renders the scene with the parameters supplied by the asset_manager
    ///and returns the future of this frame. The future can be joint to wait for the gpu
    ///or be supplied to the next update();
    pub fn render(
        &mut self,
        asset_manager: &mut asset_manager::AssetManager,
    ){
        //Show the other system that we are working
        self.set_working_cpu();

        //First of all we get info if we should debug anything, if so this bool will be true
        let (should_capture, mut time_step, start_time) = {
            let cap_bool = {
                let mut lck_set = self.engine_settings.read().expect("failed to lock settings");
                lck_set.capture_frame
            };
            let time_step = Instant::now();
            let start_time = Instant::now();
            (cap_bool, time_step, start_time)
        };


        //Update the camera data for this frame
        {
            let mut uniform_manager_lck = self.uniform_manager.write().expect("failed to lock uniform_man.");
            //Finally upadte the MVP data as well
            uniform_manager_lck.update(asset_manager.get_camera().as_uniform_data());
        }

        if should_capture{
            time_step = Instant::now();
        }

        //start the frame
        let mut command_buffer = JkCommandBuff::new(None, self.device.clone(), self.queue.clone());
        //First of all we compute the light clusters
        let light_buffer_future = self.light_system.update_light_set(
            asset_manager,
            &mut self.render_thread_pool
        );

        //Join the upload future of the light buffer
        command_buffer.join(light_buffer_future);
        if should_capture{
            let time_needed = time_step.elapsed().subsec_nanos();
            println!("\tRE: Nedded {} ms to update the light set!", time_needed as f32 / 1_000_000.0);
            time_step = Instant::now()
        }

        //Now we render all the forward stuff
        self.object_rendering.do_forward_shading(
            &self.frame_system,
            asset_manager,
            &mut command_buffer,
            &mut self.render_thread_pool,
            &mut self.debug_info
        );

        if should_capture{
            let time_needed = time_step.elapsed().subsec_nanos();
            println!("\tRE: Nedded {} ms to do forward shading!", time_needed as f32 / 1_000_000.0);
            time_step = Instant::now()
        }

        //Now execute this, since we need the gbuffer for the raytracing stage
        command_buffer.sync_queues();

        //Raytrace shadows (later reflections as well)
        self.ray_tracing.execute_shadows(
            &mut command_buffer,
            &self.frame_system,
            &self.light_system,
            asset_manager
        );

        //While raytracing, also calculate AO and store it in the ao image buffer.
        //Execute AO calculation parallel to the ray tracing step
        self.post_process.calculate_ao(
            &mut command_buffer,
            &self.frame_system,
            asset_manager
        );

        command_buffer.sync_queues();
        //After calculation AO, shade scene

        self.ray_tracing.shade_diffuse(
            &mut command_buffer,
            &self.frame_system,
            &self.light_system,
            asset_manager
        );


        //Since we fininshed the primary work on the asset manager, change to gpu working state
        self.set_working_gpu();

        //get us the next image
        let (image_number, acquire_future) = {
            match self.check_swapchain(){
                Ok(k) => {
                    k
                },
                Err(e) => {
                    println!("Could not get next swapchain image: {}", e);
                    //early return to restart the frame, fake gpu working
                    self.set_working_gpu();
                    return;
                }
            }
        };

        //Wait for the swapchain image
        command_buffer.join(Box::new(acquire_future) as Box<GpuFuture + Send + Sync>);
        //Do all post progressing and finally write the whole frame to the swapchain image
        self.post_process.do_post_process(
            &mut command_buffer,
            &self.frame_system,
            self.images[image_number].clone()
        );
        if should_capture{
            let time_needed = time_step.elapsed().subsec_nanos();
            println!("\tRE: Nedded {} ms to do final postprogress!", time_needed as f32 / 1_000_000.0);
            time_step = Instant::now()
        }

        //Execute the Post Progress portion of this frame
        command_buffer.sync_queues();

        if should_capture{
            let time_needed = time_step.elapsed().subsec_nanos();
            println!("\tRE: Nedded {} ms to finish!", time_needed as f32 / 1_000_000.0);
            time_step = Instant::now()
        }

        let mut this_frame = command_buffer.finish()
        .then_swapchain_present(self.queue.graphics.clone(), self.swapchain.clone(), image_number)
        .then_signal_fence_and_flush()
        .expect("failed to signal fences and flush");


        if should_capture{
            let time_needed = time_step.elapsed().subsec_nanos();
            println!("\tRE: Nedded {} ms to present and flush!", time_needed as f32 / 1_000_000.0);
            time_step = Instant::now()
        }


        //while the gpu is working, clean the old data
        //clean old frame
        this_frame.cleanup_finished();

        if should_capture{
            let time_needed = time_step.elapsed().subsec_nanos();
            println!("\tRE: Nedded {} ms to cleanup!", time_needed as f32 / 1_000_000.0);
            time_step = Instant::now()
        }
        //For now always wait for the gpu for real measurements
        this_frame.wait(Some(Duration::from_secs(1))).expect("failed to wait for graphics to debug");

        if should_capture{
            //also wait for the graphics card to end
            this_frame.wait(Some(Duration::from_secs(1))).expect("failed to wait for graphics to debug");
            let time_needed = time_step.elapsed().subsec_nanos();
            println!("\tRE: Nedded {} ms to wait for gpu!", time_needed as f32 / 1_000_000.0);
            //NOTE time_step = Instant::now()
        }

        //Resetting debug options
        if should_capture{
            //ait for the gput to mesure frame time
            let frame_time = start_time.elapsed().subsec_nanos();
            println!("\t RE: FrameTime: {}ms", frame_time as f32/1_000_000.0);
            println!("\t RE: Which is {}fps", 1.0/(frame_time as f32/1_000_000_000.0));
            self.debug_info.print_stat();
            self.engine_settings.write().expect("failed to lock settings").stop_capture();
        }

        //update the debug info with this frame
        self.debug_info.update();
        //Box::new(after_frame)
        //now overwrite the current future
        //self.last_frame_end = Some(this_frame)
    }

    ///UNIMPLEMENTED, however, should be used at some point to execute some compute buffer when possible on then
    /// GPU
    fn execute_cb_async(&self, cb: AutoCommandBuffer){
        //IMPLEMENT
    }

    ///Returns the uniform manager
    pub fn get_uniform_manager(&self) -> Arc<RwLock<uniform_manager::UniformManager>>{
        self.uniform_manager.clone()
    }

    ///Returns the pipeline manager of this renderer
    pub fn get_pipeline_manager(&self) -> Arc<RwLock<pipeline_manager::PipelineManager>>{
        self.pipeline_manager.clone()
    }

    ///Returns the device of this renderer
    pub fn get_device(&self) -> Arc<Device>{
        self.device.clone()
    }

    ///Returns the queue of this renderer
    pub fn get_queue(&self) -> JkQueues{
        self.queue.clone()
    }

    ///Returns an instance of the engine settings
    ///This might be a dublicate, still helpful
    pub fn get_engine_settings(&mut self) -> Arc<RwLock<engine_settings::EngineSettings>>{
        self.engine_settings.clone()
    }

    ///Returns the available renderpasses
    pub fn get_render_passes(&self) -> Arc<RwLock<RenderPasses>>{
        self.render_passes.clone()
    }

    ///Returns the current engine state
    pub fn get_render_state(&self) -> Arc<RwLock<RenderState>>{
        self.state.clone()
    }

    fn set_working_cpu(&mut self){
        let mut state_lck = self.state.write().expect("failed to lock render state");
        *state_lck = RenderState::work_cpu();
    }

    fn set_working_gpu(&mut self){
        let mut state_lck = self.state.write().expect("failed to lock render state");
        *state_lck = RenderState::work_gpu();
    }
}
